/**
 * 获取页面隐藏属性的前缀
 * 如果页面支持 hidden 属性，返回 '' 就行
 * 如果不支持，各个浏览器对 hidden 属性，有自己的实现，不同浏览器不同前缀，遍历看支持哪个
 */
function getPagePropertyPrefix() {
    const prefixes = ['webkit', 'moz', 'ms', 'o']
    let correctPrefix

    if ('hidden' in document) return ''

    prefixes.forEach((prefix) => {
        if (`${prefix}Hidden` in document) {
            correctPrefix = prefix
        }
    });

    return correctPrefix || false
}

/**
 * 获取判断页面 显示|隐藏 状态改变的属性
 */
function getVisibilityChangeProperty() {
    const prefix = getPagePropertyPrefix()
    if (prefix === false) return false

    return `${prefix}visibilitychange`
}

/**
 * 判断页面是否隐藏（进入后台）
 */
function isPageHidden() {
    const prefix = getPagePropertyPrefix()
    if (prefix === false) return false

    const hiddenProperty = prefix ? `${prefix}Hidden` : 'hidden'
    return document[hiddenProperty]
}

/**
 * 检测是否唤端成功
 * @param {function} cb - 唤端失败回调函数
 */
export function checkOpen(cb, timeout) {
    const visibilityChangeProperty = getVisibilityChangeProperty();
    const timer = setTimeout(() => {
        const hidden = isPageHidden();
        if (!hidden) {
            cb && cb()
        }
    }, timeout)

    if (visibilityChangeProperty) {
        document.addEventListener(visibilityChangeProperty, () => {
            clearTimeout(timer)
        })

        return
    }

    window.addEventListener('pagehide', () => {
        clearTimeout(timer)
    })
}

export default function (callback) {
    window.top.location.href = "vechainpro://com.vechain.user"
    checkOpen(callback, 3000)
}
