const u = navigator.userAgent
const ios = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)
const android = u.indexOf('Android') > -1
const language = ~navigator.language.indexOf('zh') ? 'cn' : 'en'

/**
 * @description 1. browser type: ios or android 2. language
 */
export default {
    isPro: /VeChainPro/.test(u),
    isIos: ios,
    isAndroid: android,
    language: language
}
