export const dateFormat = function (str, fmt) {
    const date = new Date(str)
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hours = date.getHours()
    const min = date.getMinutes()
    const second = date.getSeconds()
    const o = {
        'M+': month,
        'd+': day,
        'h+': hours,
        'm+': min,
        's+': second
    }
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (`${date.getFullYear()}`).substr(4 - RegExp.$1.length))
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : ((`00${o[k]}`).substr((`${o[k]}`).length)))
        }
    }
    return fmt
}
