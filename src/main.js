import Vue from 'vue'
import App from './App.vue'
import i18n from './i18n'
import './axios'
import '@/components/async'
import loading from './directives/loading/'
import VueLazyload from 'vue-lazyload'

require('intersection-observer')

Vue.use(VueLazyload)

Vue.use(loading)
Vue.config.productionTip = false

new Vue({
	i18n,
	render: h => h(App),
}).$mount('#app')
