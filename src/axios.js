import Axios from 'axios'
const baseURL = process.env.NODE_ENV === 'development' ? '' : process.env.VUE_APP_HOST || ''
Axios.defaults.baseURL = baseURL
Axios.defaults.timeout = 20000
Axios.defaults.withCredentials = true

Axios.interceptors.response.use(
	resp => {
		let result = resp.data
		if (result.code === 1 || result.code === 'common.success') {
			return resp.data
		} else {
			return Promise.reject(resp.data)
		}
	},
	error => {
		const response = error.response.data
		return Promise.reject(response.code)
	}
)
