import Vue from 'vue'
import Loading from '@/components/loading'

const Mask = Vue.component('iLoading', Loading)

const toggleLoading = (el, binding) => {
	let mask = el.instance.$el
	if (binding.value) {
		Vue.nextTick(() => {
			mask.style.visibility = ''
			el.appendChild(mask)
		})
	} else {
		mask.style.visibility = 'hidden'
	}
}

export default {
	bind: function (el, binding, value) {
		const mask = new Mask({
			el: el
		})
		el.instance = mask

		binding.value && toggleLoading(el, binding)
	},
	update: function (el, binding) {
		if (binding.oldValue !== binding.value) {
			toggleLoading(el, binding)
		}
	},
	unbind: function (el, binding) {
		el.instance && el.instance.$destroy()
	}
}
