const app = {
	product_info: "产品信息",
	txid: "区块链记录: ",
	show_info: "更多信息",
	track_info: "溯源信息",
	product_head: "产品信息",
	show_head: "更多信息",
	track_head: "溯源信息",
	valid_msg: "此产品部分信息已被DNVGL认证",
	message_tc_landingpage_NotAllowException: "请联系管理员公开此信息",
	message_tc_landingpage_DataEmptyException: "芯片验真失败",
	message_default: "无法读取产品信息",
	sorry: "抱歉",
	download_msg: "建议使用VeChain Pro查看芯片验真及产品完整信息",
	download: "下载App",
	mainnet: "正试",
	testnet: "测试"
}

export default {
	app
}
