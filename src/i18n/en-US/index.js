
const app = {
	product_info: "Product information",
	txid: "Tx record: ",
	show_info: "More information",
	track_info: "Traceability information",
	product_head: "Product",
	show_head: "More",
	track_head: "Traceability",
	valid_msg: "Part of the production process has been verified by DNV-GL",
	message_tc_landingpage_NotAllowException: "Please contact your admin to open the access for the information",
	message_tc_landingpage_DataEmptyException: "Fail authentication",
	message_default: "Unreadable product information",
	sorry: "Error",
	download_msg: "You may check the verification information and more details of the product by using VeChain Pro.",
	download: "Download",
	mainnet: "Main",
	testnet: "Test"
}

export default {
	app
}
