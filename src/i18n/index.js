import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './en-US/'
import cn from './zh-CN/'

Vue.use(VueI18n)

const lang = window.navigator.language.indexOf('zh') > -1 ? 'zh-CN' : 'en-US' // 浏览器语言
const i18n = new VueI18n({
  locale: lang,
  messages: {
    'en-US': en,
		'zh-CN': cn
  }
})
export default i18n

