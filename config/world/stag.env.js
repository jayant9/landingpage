'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: 'staging',
  HOST_ENV: {
	  VUE_APP_HOST: 'https://v-baas-ali-hk-deploy.vtho.net/v1/data',
	  VUE_APP_INTERNAL_HOST: 'https://toolchain-operator-deploy.vtho.net/operatorapi/vid/preview/types'
  }
})
