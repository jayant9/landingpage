'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: 'test',
  HOST_ENV: {
	  VUE_APP_HOST: 'https://v3-dev.vechaindev.com/v1/data',
	  VUE_APP_INTERNAL_HOST: 'https://toolchain-operator-test.vtho.net/operatorapi/vid/preview/types'
  }
})
