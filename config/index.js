function getHost(process) {
	const {REGION, ENV, CODE, INTERNAL} = chkEnv()

	process.stdout.write(`build for ${REGION} of ${ENV} from ${CODE}. is internal? ${INTERNAL} \n`);
	// require指定的环境配置文件
	const envConfigFile = "../config/" + REGION + '/' + ENV + ".env.js";
	process.stdout.write('the env config file is '+ envConfigFile +'\n');
	var config = require(envConfigFile)
	const {VUE_APP_INTERNAL_HOST, VUE_APP_HOST} = config.HOST_ENV
	return {
		VUE_APP_HOST: INTERNAL ? VUE_APP_INTERNAL_HOST : VUE_APP_HOST 
	}
	// const 
	// const versionIndex = argv.indexOf('--version')
	// if (versionIndex > -1) {
	// 	let versionKey = argv[versionIndex + 1]
	// 	const versionVal = env[`VUE_APP_${(versionKey || '').toUpperCase()}_HOST`]
	// 	if (!versionKey || !versionVal) {
	// 		throw new Error('please pass correct version value')
	// 	}
	// 	return {
	// 		VUE_APP_HOST: versionVal
	// 	}
	// }
	// return {
	// 	VUE_APP_HOST: env['VUE_APP_HOST']
	// }
}

function chkEnv(){
	var gtEnv = 'local'
	switch(process.env.NODE_ENV){
	  case 'dev':
	  case 'development':
	    gtEnv = 'dev'
	    break;
	  case 'test' :
	  case 'testing':
	    gtEnv = 'test'
	    break;
	  case 'prod':
	  case 'production':
	    gtEnv = 'prod'
	    break;
	  case 'stag':
	  case 'staging':
	    gtEnv = 'stag'
	    break;
	}
	return {
		REGION: ['china', 'world'] .indexOf(process.env.REGION) !== -1 ? process.env.REGION : 'world',
		ENV: gtEnv,
		CODE: process.env.NODE_ENV,
		INTERNAL: process.env.APP_NAME === 'toolchain-landingpage-classic-internal' // toolchain-landingpage-classic-internal || toolchain-landingpage-classic-internal : 
	}
}

module.exports = getHost