'use strict'
module.exports = {
  NODE_ENV: 'production',
  HOST_ENV: {
	  VUE_APP_HOST: 'https://v.vechain.com/v1/data',
	  VUE_APP_INTERNAL_HOST: 'https://workspace.vetoolchain.cn/operatorapi/vid/preview/types'
  }
}
