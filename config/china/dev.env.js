'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: 'development',
  HOST_ENV: {
	  VUE_APP_HOST: 'https://v-dev.vechaindev.com/v1/data',
		VUE_APP_INTERNAL_HOST: 'https://toolchain-operator-dev.vechaindev.com/operatorapi/vid/preview/types'
  }
})
