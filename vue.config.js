const getHost = require('./config')
const output = process.env.VUE_APP_OUTPUTDIR || 'classic'
const VUE_APP_HOST = getHost(process)['VUE_APP_HOST']
process.env.NODE_ENV = 'production'
module.exports = {
	outputDir: 'dist/',
	publicPath: process.env.NODE_ENV === "production" ? "./" : "./",
	productionSourceMap: true,
	css: {
		loaderOptions: {
			postcss: {
				plugins: [
					require("postcss-px-to-viewport")({
						unitToConvert: "px",
						viewportWidth: 375,
						unitPrecision: 3,
						propList: [
							"*"
						],
						viewportUnit: "vw",
						fontViewportUnit: "vw",
						selectorBlackList: ['video-js'],
						minPixelValue: 1,
						mediaQuery: false,
						replace: true,
						exclude: /(\/|\\)(node_modules)(\/|\\)/,
					}),
					require('autoprefixer')({
						"overrideBrowserslist": [
							"Android >= 4.0",
							"iOS >= 7"
						]
					})
				]
			}
		}
	},
	configureWebpack: config => {
		if (process.env.NODE_ENV === "development") {
			config.devtool = "cheap-module-eval-source-map"
		} else {
			config.devtool = "cheap-module-source-map"
		}
	},
	chainWebpack: config => {
		config
			.plugin('define')
			.tap(args => {
				args[0]['process.env']['VUE_APP_HOST'] = JSON.stringify(VUE_APP_HOST)
				// Object.assign(args[0]['process.env'], { VUE_APP_HOST })
				// console.log(args[0])
				return args
			})
		config.plugin('html').tap(args => {
			// args[0].cdn = {
			// 	js: [
			// 		'https://api.map.baidu.com/api?v=2.0&ak=GHFYo7NYYDwKHgKuwiz584lWO9T8xyPS'
			// 	]
			// }
			return args
		})
		if (process.env.NODE_ENV === 'production') {
			config.optimization.splitChunks({
				chunks: "all",
				cacheGroups: {
					libs: {
						name: "chunk-libs",
						test: /[\\/]node_modules[\\/]/,
						priority: 10,
						chunks: "initial" // 只打包初始时依赖的第三方b
					},
					videoplayer: {
						name: "chunk-video", // 单独将 elementUI 拆包
						priority: 20, // 权重要大于 libs 和 app 不然会被打包进 libs 或者 app
						test: /[\\/]node_modules[\\/]video\.js[\\/]/
					}
				}
			})
			if (process.env.npm_config_report) {
				config
					.plugin('webpack-bundle-analyzer')
					.use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
					.end()
				config.plugins.delete('prefetch')
			}
		}
	},
	parallel: require('os').cpus().length > 1,
	devServer: {
		host: '0.0.0.0',
		port: 8080,
		https: false,
		hotOnly: false
	}
}
